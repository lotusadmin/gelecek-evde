using System;

namespace gelecek.Web.Requests.Model
{
    public class SubscribePostDto
    {
        public int id { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }
        public DateTime SendDate { get; set;}
    }
}