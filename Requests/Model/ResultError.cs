﻿namespace gelecek.Web.Requests.Model
{
    public enum ResultError
    {
        None,
        NotFound,
        InUse
    }
}