using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using gelecek.Web.Data;
using gelecek.Web.Requests.Model;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace gelecek.Web.Requests {
    public static class Subscribe {

        public class GetSubscribes : IRequest<ResultDto<SubscribeDto[]>> { }
        public class GetSubscribebyId : IRequest<ResultDto<SubscribeDto>> {
            public int Id { get; set; }
        }

        public class SaveSubscribe : IRequest<ResultDto<SubscribeDto>> {
            public int Id { get; set; }
            public SubscribePostDto Post { get; set; }
        }

        public class Handler : IRequestHandler<GetSubscribebyId, ResultDto<SubscribeDto>>,
            IRequestHandler<SaveSubscribe, ResultDto<SubscribeDto>>,
            IRequestHandler<GetSubscribes, ResultDto<SubscribeDto[]>> {
                private readonly gelecekDbContext _gelecekcontext;
                private readonly IMapper _mapper;

                public Handler (gelecekDbContext context, IMapper mapper) {
                    _gelecekcontext = context ??
                        throw new ArgumentNullException (nameof (context));
                    _mapper = mapper ??
                        throw new ArgumentNullException (nameof (mapper));
                }
                public async Task<ResultDto<SubscribeDto>> Handle (GetSubscribebyId request, CancellationToken cancellationToken) {
                    var result = new ResultDto<SubscribeDto> ();
                    if (request.Id <= default (int)) throw new ArgumentNullException (nameof (request.Id));

                    var entity = await _gelecekcontext.Subscribe.FirstOrDefaultAsync (x => x.Id == request.Id, cancellationToken : cancellationToken);

                    if (entity != null) {
                        result.Result = _mapper.Map<SubscribeDto> (entity);
                    } else {
                        result.Error = ResultError.NotFound;
                    }

                    return result;
                }
                public async Task<ResultDto<SubscribeDto>> Handle (SaveSubscribe request, CancellationToken cancellationToken) {
                    var result = new ResultDto<SubscribeDto> ();
                    if (request.Post == null) throw new ArgumentNullException (nameof (request.Post));

                    var entity = _mapper.Map<Data.Model.Subscribe> (request.Post);

                    if (request.Id <= default (int)) {
                        _gelecekcontext.Subscribe.Add (entity);
                    } else {
                        entity.Id = request.Id;
                        _gelecekcontext.Subscribe.Update (entity);
                    }

                    await _gelecekcontext.SaveChangesAsync (cancellationToken);
                    result.Result = _mapper.Map<SubscribeDto> (entity);

                    return result;
                }
                public async Task<ResultDto<SubscribeDto[]>> Handle (GetSubscribes request, CancellationToken cancellationToken) {
                    var result = new ResultDto<SubscribeDto[]> ();

                    var list = _gelecekcontext.Subscribe.AsQueryable ();

                    result.Result = await list.Select (x => _mapper.Map<SubscribeDto> (x)).ToArrayAsync ();

                    return result;
                }
            }
    }
}