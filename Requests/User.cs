using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using gelecek.Web.Data;
using gelecek.Web.Requests.Model;

namespace gelecek.Web.Requests
{
    public class User
    {
        public class GetEmailConfirmedUserList : IRequest<ResultDto<UserDto[]>>
        {
            
        }

        public class Handler :  IRequestHandler<GetEmailConfirmedUserList, ResultDto<UserDto[]>>
        {
            private readonly gelecekDbContext _nestlecontext;
            private readonly IMapper _mapper;

            public Handler(gelecekDbContext context, IMapper mapper)
            {
                _nestlecontext = context ?? throw new ArgumentNullException(nameof(context));
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }
            
            public async Task<ResultDto<UserDto[]>> Handle(GetEmailConfirmedUserList request, CancellationToken cancellationToken)
            {               
                var result = new ResultDto<UserDto[]>();
                
                var list = _nestlecontext.Users.Where(x => x.EmailConfirmed).AsQueryable();
                
                result.Result = await list.Select(x=> _mapper.Map<UserDto>(x)).ToArrayAsync(cancellationToken);

                return result;
            }
        }
    }
}