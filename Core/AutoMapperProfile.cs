﻿using AutoMapper;
using gelecek.Web.Data.Model;
using gelecek.Web.Requests.Model;
using gelecek.Web.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace gelecek.Web.Core {
    public class AutoMapperProfile : Profile {
        public AutoMapperProfile () {
            CreateMap<Email, EmailDto> ();
            CreateMap<EmailPostDto, Email> ()
                .ForMember (m => m.Id, m => m.Ignore ());
            CreateMap<Subscribe, SubscribeDto> ();
            CreateMap<SubscribePostDto, Subscribe> ()
                .ForMember (m => m.Id, m => m.Ignore ());
            // Nestle Tables

            CreateMap<ApplicationUser, UserViewModel> ()
                .ForMember (d => d.Roles, map => map.Ignore ());
            CreateMap<UserViewModel, ApplicationUser> ();

            CreateMap<ApplicationUser, UserEditViewModel> ()
                .ForMember (d => d.Roles, map => map.Ignore ());
            CreateMap<UserEditViewModel, ApplicationUser> ();

            CreateMap<ApplicationUser, UserPatchViewModel> ()
                .ReverseMap ();

            CreateMap<IdentityRole, RoleViewModel> ()
                //                .ForMember(d => d.Permissions, map => map.MapFrom(s => s.Claims))
                //                .ForMember(d => d.UsersCount, map => map.ResolveUsing(s => s.Users?.Count ?? 0))
                .ReverseMap ();
            CreateMap<RoleViewModel, IdentityRole> ();

            CreateMap<IdentityRoleClaim<string>, ClaimViewModel> ()
                .ForMember (d => d.Type, map => map.MapFrom (s => s.ClaimType))
                .ForMember (d => d.Value, map => map.MapFrom (s => s.ClaimValue))
                .ReverseMap ();

            CreateMap<ApplicationPermission, PermissionViewModel> ()
                .ReverseMap ();

            CreateMap<IdentityRoleClaim<string>, PermissionViewModel> ()
                .ConvertUsing (s => Mapper.Map<PermissionViewModel> (ApplicationPermissions.GetPermissionByValue (s.ClaimValue)));
        }
    }
}