﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gelecek.Web.Migrations
{
    public partial class sendDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "sendDate",
                table: "gelecek.Subscribe",
                newName: "SendDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SendDate",
                table: "gelecek.Subscribe",
                newName: "sendDate");
        }
    }
}
