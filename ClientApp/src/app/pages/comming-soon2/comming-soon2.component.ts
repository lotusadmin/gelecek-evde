import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { load } from 'recaptcha-v3';
import { LotusService, AlertService } from 'src/app/shared/service';
import { Subscribe } from 'src/app/models/subscribe.model';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-comming-soon2',
  templateUrl: './comming-soon2.component.html',
  styleUrls: ['./comming-soon2.component.scss'],
  providers: [AlertService]
})
export class CommingSoon2Component implements OnInit, OnDestroy {

  public seconds = 0;
  public timer: any;
  public element: HTMLElement;
  public days = 0;
  public hours = 0;
  public minutes = 0;
  public emailAddressString: string;
  public successGotEmail = false;
  validation = false;

  // Audio settings,
  msbapTitle = 'Parlament Sinema Kulübü';
  msbapAudioUrl = '../../../assets/video/parliament.mp3';

  msbapDisplayTitle = false;
  msbapDisplayVolumeControls = true;


  constructor(private alertService: AlertService, private lotusService: LotusService,
    @Inject(DOCUMENT) private document: Document) {
    this.setTime();
  }

  ngOnInit() {
    load('6Le6sbEZAAAAANtUTB6e7w2fzeKpkMaZIH0A3aRt').then((recaptcha) => {
      recaptcha.execute('homepage').then((token) => {
        // console.log(token) // Will print the token
      })
    });
  }
  onEnded(e: any) {

  }
  setTime() {

    this.timer = setInterval(() => {
      const now = new Date().getTime();
      let distance = new Date().getTime()
      const countDown = new Date('07.29.2020 21:43:00').getTime();
      distance = countDown - now;
      console.log(' this.countDown :' + countDown);
      this.days = Math.floor(distance / (1000 * 60 * 60 * 24));
      this.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      this.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      this.seconds = Math.floor((distance % (1000 * 60)) / 1000);
    }, 1000);
  }

  sendEmailaddress() {
    const em: Subscribe = new Subscribe();
    if (this.emailAddressString === null || this.emailAddressString.length === 0) {
      this.validation = true;
      return;
    }
    em.type = 'email';
    em.email = this.emailAddressString;
    em.sendDate = new Date();
    this.lotusService.AddEmail(em).subscribe(x => {
      if (x.isSuccessfull) {
        this.alertService.showNotification('bottom', 'right', this.emailAddressString + ' ile emailiniz kaydedilmiştir.', 1);
        this.successGotEmail = true;
        setTimeout(() => {
          this.successGotEmail = false;
        }, 4000);
      }
    });
  }



  ngOnDestroy() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }


  gotoEvent(): void {
    this.document.location.href = 'https://www.mobilet.com/event/gelecek-5151-a3ee';
  }


}
