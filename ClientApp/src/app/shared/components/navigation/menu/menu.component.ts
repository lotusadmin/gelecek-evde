import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Menu, NavService } from '../../../service/nav.service';
import { AuthService } from 'src/app/shared/service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, AfterViewInit {
  public menuItems: Menu[];
  public openSide = false;
  public activeItem = 'home';
  public active = false;
  public activeChildItem = '';
  public overlay = false;
  public loginflag = false;
  constructor(public navServices: NavService, private authService: AuthService, private router: Router,
    public location: Location) {

  }

  ngOnInit() {


    this.loginflag = this.authService.isLoggedIn;

  }
  ngAfterViewInit() {
    this.navServices.callMenuItems();
    this.navServices.items.subscribe(menuItems => {
      this.menuItems = menuItems
      console.log('this.menuItems ' + JSON.stringify(this.menuItems));
    });
  }

  toggleSidebar() {
    this.openSide = !this.openSide
  }

  closeOverlay() {
    this.openSide = false
  }

  // For Active Main menu in Mobile View
  setActive(menuItem) {
    if (this.activeItem === menuItem) {
      this.activeItem = ''
    } else {
      this.activeItem = menuItem
    }
  }

  isActive(item) {
    return this.activeItem === item
  }

  // For Active Child Menu in Mobile View
  setChildActive(subMenu) {
    if (this.activeChildItem === subMenu) {
      this.activeChildItem = ''
    } else {
      this.activeChildItem = subMenu
    }
  }

  ischildActive(subMenu) {
    return this.activeChildItem === subMenu
  }

  logout() {
    this.authService.logout();
    this.authService.redirectLogoutUser();
  }

}
