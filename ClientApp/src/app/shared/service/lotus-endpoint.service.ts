import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';
import { Email } from '../../models';
import { NumberFormatStyle, CurrencyPipe } from '@angular/common';
import { Subscribe } from 'src/app/models/subscribe.model';

@Injectable()
export class LotusEndpoint extends EndpointFactory {

  private readonly TuserListUrl: string = '/api/account/users';
  private readonly TEmails: string = '/api/email';
  private readonly TSubsc: string = '/api/Subscribe';
  get userListUrl() { return this.configurations.baseUrl + this.TuserListUrl; }
  get emailsUrl() { return this.configurations.baseUrl + this.TEmails; }
  get subscribeUrl() { return this.configurations.baseUrl + this.TSubsc; }
  // Gelecek Links

  constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {
    super(http, configurations, injector);
  }

  sendEmailEndpoint<T>(EmailT?: Email): Observable<T> {
    const endpointUrl = this.emailsUrl
    const body = JSON.stringify(EmailT);
    return this.http.post<T>(endpointUrl, body, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.sendEmailEndpoint<T>(EmailT));
      });
  }

  addEmailEndpoint<T>(EmailT?: Subscribe): Observable<T> {
    const endpointUrl = this.subscribeUrl
    const body = JSON.stringify(EmailT);
    return this.http.post<T>(endpointUrl, body, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.addEmailEndpoint<T>(EmailT));
      });
  }


  getAllEmails<T>(): Observable<T> {
    return this.http.get<T>(this.emailsUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getAllEmails());
      });
  }



  getUserListEndpoint<T>(): Observable<T> {
    return this.http.get<T>(this.userListUrl, this.getRequestHeaders())
      .catch(error => {
        return this.handleError(error, () => this.getUserListEndpoint());
      });
  }



}
