
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanLoad, Route } from '@angular/router';
import { AuthService } from './auth.service';
import { AccountService } from './account.service';


@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  isAdmin: boolean;
  constructor(private authService: AuthService, private router: Router, private Account: AccountService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log(' route:::' + route);
    const currentUser = this.authService.currentUser;
    if (currentUser) {
        // check if route is restricted by role
       // console.log(' USER ROLES' + JSON.stringify(currentUser.roles));
        if (route.data.roles && route.data.roles.indexOf(currentUser.roles[0]) === -1) {
            // role not authorised so redirect to home page
            this.router.navigate(['/']);
            return false;
        }
      }
    const url: string = state.url;
    return this.checkLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): boolean {

    const url = `/${route.path}`;
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {

    if (this.authService.isLoggedIn) {
      return true;
    }

    this.authService.loginRedirectUrl = url;
    this.router.navigate(['/login']);

    return false;
  }
}
