import { Component, OnInit, Injectable } from '@angular/core';
import { Email } from '../../models';
import { LotusService } from './lotus.service';
declare var $: any;

@Injectable()
export class EmailService {
    constructor(public lotusService: LotusService) { }
    sendEmail(to, subject, body) {
        if (to === '' || subject === '' || body === '') {
            return;
        }
        const EmailObject = new Email();
        EmailObject.to = to
        EmailObject.subject = subject
        EmailObject.body = body

        this.lotusService.sendEmail(EmailObject).subscribe((email) => {
            if (email.isSuccessfull) {

            }
        });

    }

}
