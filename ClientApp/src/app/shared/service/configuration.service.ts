import { Injectable } from '@angular/core';

import { LocalStoreManager } from './local-store-manager.service';
import { DBkeys } from './db-keys';
import { Utilities } from './utilities';
import { environment } from '../../../environments/environment';

interface UserConfiguration {
  homeUrl: string;
}

@Injectable()
export class ConfigurationService {

  set homeUrl(value: string) {
    this.homeUrlT = value;
    this.saveToLocalStore(value, DBkeys.HOME_URL);
  }

  get homeUrl() {
    if (this.homeUrlT != null) {
      return this.homeUrlT;
    }

    return ConfigurationService.defaultHomeUrl;
  }

  public static readonly appVersion: string = '1.0';

  public static readonly defaultHomeUrl: string = '/';

  public baseUrl = environment.baseUrl || Utilities.baseUrl();
  public loginUrl = environment.loginUrl;
  public fallbackBaseUrl = 'http://localhost:5000';

  private homeUrlT: string = null;

  constructor(private localStorage: LocalStoreManager) {
    this.loadLocalChanges();
  }

  private loadLocalChanges() {

    if (this.localStorage.exists(DBkeys.HOME_URL)) {
      this.homeUrlT = this.localStorage.getDataObject<string>(DBkeys.HOME_URL);
    }
  }

  private saveToLocalStore(data: any, key: string) {
    setTimeout(() => this.localStorage.savePermanentData(data, key));
  }

  public import(jsonValue: string) {

    this.clearLocalChanges();

    if (!jsonValue) {
      return;
    }

    const importValue: UserConfiguration = Utilities.JSonTryParse(jsonValue);

    if (importValue.homeUrl != null) {
      this.homeUrl = importValue.homeUrl;
    }
  }

  public export(changesOnly = true): string {

    const exportValue: UserConfiguration = {
      homeUrl: changesOnly ? this.homeUrlT : this.homeUrl
    };

    return JSON.stringify(exportValue);
  }

  public clearLocalChanges() {
    this.homeUrlT = null;
    this.localStorage.deleteData(DBkeys.HOME_URL);
  }
}
