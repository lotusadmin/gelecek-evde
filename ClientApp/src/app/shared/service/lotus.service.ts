import { Injectable } from '@angular/core';
import { Observable, Subscribable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { LotusEndpoint } from './lotus-endpoint.service';
import { AuthService } from './auth.service';
import { ApiResult, Email, User } from '../../models';
import { Subscribe } from 'src/app/models/subscribe.model';



@Injectable()
export class LotusService {

  private subject = new Subject<any>();

  constructor(private authService: AuthService, private lotusEndpoint: LotusEndpoint) { }


  sendMessage(message: string) {
    this.subject.next({ text: message });
  }

  clearMessage() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  getEmails() {
    return this.lotusEndpoint.getAllEmails<ApiResult<Email[]>>();
  }

  sendEmail(email: Email) {
    return this.lotusEndpoint.sendEmailEndpoint<ApiResult<Email>>(email);
  }

  AddEmail(email: Subscribe) {
    return this.lotusEndpoint.addEmailEndpoint<ApiResult<Email>>(email);
  }

  get currentUser() {
    return this.authService.currentUser;
  }

  // WR : OK
  // gets all Users.
  // 11.03.2019
  getUserList() {
    return this.lotusEndpoint.getUserListEndpoint<ApiResult<User[]>>();
  }
}
