import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountEndpoint } from './account-endpoint.service';
import { AccountService } from './account.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { ConfigurationService } from './configuration.service';
import { EndpointFactory } from './endpoint-factory.service';
import { LocalStoreManager } from './local-store-manager.service';

import { LotusEndpoint } from './lotus-endpoint.service';
import { LotusService } from './lotus.service';
import { EmailService } from './email.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    AccountEndpoint,
    AccountService,
    AuthService,
    AuthGuard,
    ConfigurationService,
    EndpointFactory,
    LocalStoreManager,
    LotusEndpoint,
    LotusService,
    EmailService
  ],
})
export class ServicesModule { }
