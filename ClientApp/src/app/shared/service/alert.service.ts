import { Injectable } from '@angular/core';
declare var $: any;

@Injectable()
export class AlertService {
    [x: string]: any;
    constructor() { }
    showNotification(from, align, htmlString, color) {

        const type = ['', 'info', 'success', 'warning', 'danger'];

        $.notify({
            icon: 'notifications',
            message: htmlString
        }, {
            type: type[color],
            timer: 500,
            placement: {
                from,
                align
            },
            // tslint:disable-next-line:max-line-length
            template:
                '<div data-notify="container"  class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-info rounded" role="alert">' +
                // tslint:disable-next-line:max-line-length
                '<a class="bg-transparen" data-notify="dismiss"><i class="fa fa-window-close fa-sm"></i></a>' +
                '<i class=" nc-icon nc-bell-55"></i>' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }
}
