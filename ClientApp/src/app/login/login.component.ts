import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { load } from 'recaptcha-v3';
import { AuthService, ConfigurationService, Utilities } from '../shared/service'

import { UserLogin } from '../models';
import { AlertService } from '../shared/service/alert.service';
import { NavService } from '../shared/service/nav.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AlertService, AuthService, ConfigurationService]
})

export class LoginComponent implements OnInit, OnDestroy {
  userLogin = new UserLogin();
  isLoading = false;
  formResetToggle = true;
  modalClosedCallback: () => void;
  loginStatusSubscription: any;
  test: Date = new Date();
  focus;
  focus1;
  @Input()
  public images: any;
  isModal = false;

  constructor(private alertService: AlertService,
    private authService: AuthService,
    private router: Router,
    public navServices: NavService) {
  }

  ngOnInit() {
    this.userLogin.rememberMe = this.authService.rememberMe;
    load('6LfnfscUAAAAAAJE9utbe8bHXNQXEjF35lBMQimM').then((recaptcha) => {
      recaptcha.execute('homepage').then((token) => {
        // console.log(token) // Will print the token
      })
    });
    if (this.getShouldRedirect()) {
      this.authService.redirectLoginUser();
    } else {
      this.loginStatusSubscription = this.authService.getLoginStatusEvent().subscribe(isLoggedIn => {
        if (this.getShouldRedirect()) {
          this.authService.redirectLoginUser();
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.loginStatusSubscription) {
      this.loginStatusSubscription.unsubscribe();
    }
  }


  getShouldRedirect() {
    return !this.isModal && this.authService.isLoggedIn && !this.authService.isSessionExpired;
  }


  closeModal() {
    if (this.modalClosedCallback) {
      this.modalClosedCallback();
    }
  }


  login() {
    this.isLoading = true;
    this.authService.login(this.userLogin.email, this.userLogin.password, this.userLogin.rememberMe)
      .subscribe(
        user => {
          setTimeout(() => {
            this.reset();
            if (!this.isModal) {
              this.alertService.showNotification('bottom', 'right', `Hoşgeldin ${user.userName}!`, 1);
              this.navServices.callMenuItems();
              this.router.navigate(['/page/commingsoon2']);
              // if (this.authService.currentUser.email === 'nestle@lotusopt.com') {
              // }
            } else {
              this.alertService.showNotification('bottom', 'right', `Oturum ${user.userName} restored!`, 1);
              setTimeout(() => {
                this.alertService.showNotification('bottom', 'right', 'Bi hata mı oldu ne ?  tekrar dener misin ? ', 1);
              }, 500);
            }
          }, 500);
        },
        error => {
          this.isLoading = false;
          if (Utilities.checkNoNetwork(error)) {
            // this.alertService.showToast(Utilities.noNetworkMessageCaption,
            // Utilities.noNetworkMessageDetail, MessageSeverity.error, true);
            this.alertService.showNotification('bottom', 'right', Utilities.noNetworkMessageCaption, 4);
            this.offerAlternateHost();
          } else {
            const errorMessage = Utilities.findHttpResponseMessage('error_description', error);

            if (errorMessage) {
              this.alertService.showNotification('bottom', 'right', '<h2>Giriş Sağlanamadı</h2>', 4);
            } else {
              this.alertService.showNotification('bottom', 'right', 'Bir bağlantı hatası oluştu. Lütfen sonra tekrar deneyiniz. ', 4);
            }
          }

          setTimeout(() => {
          }, 500);
        });
  }


  offerAlternateHost() {

    // if (Utilities.checkIsLocalHost(location.origin) && Utilities.checkIsLocalHost(this.configurations.baseUrl)) {
    //   // this.alertService.showDialog('Dear Developer!\nIt appears your backend Web API service is not running...\n' +
    //     // 'Would you want to temporarily switch to the online Demo API below?(Or specify another)',
    //     // DialogType.prompt,
    //     (value: string) => {
    //       this.configurations.baseUrl = value;
    //       // this.alertService.showStickyMessage('API Changed!',
    // 'The target Web API has been changed to: ' + value, MessageSeverity.warn);
    //     },
    //     null,
    //     null,
    //     null,
    //     this.configurations.fallbackBaseUrl);
    // }
  }


  reset() {
    this.formResetToggle = false;

    setTimeout(() => {
      this.formResetToggle = true;
    });
  }

  toRegisterForm() {
    this.alertService.showNotification('bottom', 'right', 'Not ready yet!', 3);
    this.router.navigate(['/login']);
  }

}
