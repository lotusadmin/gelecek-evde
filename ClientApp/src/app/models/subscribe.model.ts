

export class Subscribe {
  id: number;
  type: string;
  email: string;
  sendDate: Date;
}
