export class ApiResult<T> {
  result: T;
  error: number;
  isSuccessfull: boolean;
}
