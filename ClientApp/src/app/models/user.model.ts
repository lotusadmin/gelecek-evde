export class User {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  public id: string;
  public userName: string;
  public fullName: string;
  public email: string;
  public jobTitle: string;
  public phoneNumber: string;
  public profileImage: string;
  public isEnabled: boolean;
  public isLockedOut: boolean;
  public roles: string[];
  constructor(id?: string, userName?: string,
    fullName?: string, email?: string, jobTitle?:
      string, phoneNumber?: string, roles?: string[], profileImage?: string) {

    this.id = id;
    this.userName = userName;
    this.fullName = fullName;
    this.email = email;
    this.jobTitle = jobTitle;
    this.phoneNumber = phoneNumber;
    this.profileImage = profileImage;
    this.roles = roles;
  }
  get friendlyName(): string {
    let name = this.fullName || this.userName;

    if (this.jobTitle) {
      name = this.jobTitle + ' ' + name;
    }
    return name;
  }


}
