
import { Permission } from './permission.model';


export class Role {
  public id: string;
  public name: string;
  public description: string;
  public usersCount: string;
  public permissions: Permission[];
  constructor(name?: string, description?: string, permissions?: Permission[]) {

    this.name = name;
    this.description = description;
    this.permissions = permissions;
  }


}
