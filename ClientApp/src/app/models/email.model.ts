export class Email {
  id: number;
  senddate: Date;
  to: string;
  subject: string;
  body: string;
}
