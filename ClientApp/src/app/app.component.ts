import { Component, OnInit } from '@angular/core';
import { CustomizerService } from './shared/service/customizer.service';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from './shared/service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public url: any;
  title = 'Gelecek Evde';
  public layoutType = 'dark';

  loginflag = false;

  constructor(public customize: CustomizerService, private authService: AuthService, private router: Router,
    public location: Location) {

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.url = event.url;
      }
    });
    if (authService.isLoggedIn) {
      router.navigate(['/page/commingsoon2']);
    }
  }

  ngOnInit() {
    this.isLoggedIn();
    console.log(' LOGIN ' + this.loginflag);
  }

  isLoggedIn() {
    if (this.authService.isLoggedIn) {
      this.loginflag = true;
      console.log(' WELCOME : ) ');
    } else {
      this.loginflag = false;
      if (!this.isLogin()) {
        console.log(' Not in : ) ');
        this.router.navigate(['/page/commingsoon2']);
      }
    }
  }

  isLogin() {
    let titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }
    console.log('titlee ' + titlee);
    if (titlee === '/login') {
      return true;
    } else {
      return false;
    }
  }
  customizeLayoutVersion(val) {
    this.customize.setLayoutVersion(val);
    this.layoutType = val;
  }

  customizeLayoutType(val) {
    this.customize.setLayoutType(val);
    this.layoutType = val;
  }


}
