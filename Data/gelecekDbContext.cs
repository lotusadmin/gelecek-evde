using gelecek.Web.Data.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OpenIddict.EntityFrameworkCore.Models;

namespace gelecek.Web.Data {
    public class gelecekDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string> {
        public DbSet<Email> Email { get; set; }
        public DbSet<Subscribe> Subscribe { get; set; }
        public gelecekDbContext (DbContextOptions<gelecekDbContext> options) : base (options) { }

        protected override void OnModelCreating (ModelBuilder builder) {
            base.OnModelCreating (builder);

            builder.Entity<ApplicationUser> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<ApplicationUser> (entity => entity.Property (m => m.NormalizedEmail).HasMaxLength (85));
            builder.Entity<ApplicationUser> (entity => entity.Property (m => m.NormalizedUserName).HasMaxLength (85));
            builder.Entity<IdentityRole> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<IdentityRole> (entity => entity.Property (m => m.NormalizedName).HasMaxLength (85));
            builder.Entity<IdentityUserLogin<string>> (entity => entity.Property (m => m.LoginProvider).HasMaxLength (85));
            builder.Entity<IdentityUserLogin<string>> (entity => entity.Property (m => m.ProviderKey).HasMaxLength (85));
            builder.Entity<IdentityUserLogin<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityUserRole<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityUserRole<string>> (entity => entity.Property (m => m.RoleId).HasMaxLength (85));
            builder.Entity<IdentityUserToken<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityUserToken<string>> (entity => entity.Property (m => m.LoginProvider).HasMaxLength (85));
            builder.Entity<IdentityUserToken<string>> (entity => entity.Property (m => m.Name).HasMaxLength (85));
            builder.Entity<IdentityUserClaim<string>> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<IdentityUserClaim<string>> (entity => entity.Property (m => m.UserId).HasMaxLength (85));
            builder.Entity<IdentityRoleClaim<string>> (entity => entity.Property (m => m.Id).HasMaxLength (85));
            builder.Entity<IdentityRoleClaim<string>> (entity => entity.Property (m => m.RoleId).HasMaxLength (85));
            builder.Entity<OpenIddictAuthorization<int>> (entity => {
                entity.Property (model => model.Subject).HasMaxLength (250);
            });
            builder.Entity<OpenIddictToken<int>> (entity => {
                entity.Property (model => model.Subject).HasMaxLength (250);
            });
        }
    }
}