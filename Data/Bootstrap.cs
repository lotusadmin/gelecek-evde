using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using gelecek.Web.Core;
using gelecek.Web.Data.Model;

namespace gelecek.Web.Data
{
    public class Bootstrap
    {
        private readonly gelecekDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public Bootstrap(gelecekDbContext dbContext,  UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task SeedAsync()
        {
            await InitializeDatabase();
            await SeedRoles();
            await SeedUsers();
           
            // await SeedDomain();
        }
        
        // private async Task SeedDomain()
        // {
        //     if (EnumerableExtensions.Any(_dbContext.Grup)) return;
                
        //     var grup = await CreateGrup();
        //     await CreateBirim(grup);
        //     await CreateKategori(grup);
        //     await CreateUretici(grup);
        //     await _dbContext.SaveChangesAsync();
        //     await CreateUrun(grup);
        //     await _dbContext.SaveChangesAsync();
        //     await CreateDonem(grup);
        //     await _dbContext.SaveChangesAsync();
        //     await CreateSiparis(grup);
        //     await _dbContext.SaveChangesAsync();
        // }

       
        private async Task SeedUsers()
        {
            await AddUser("admin@gelecek.com", Roles.Administrator);
            await AddUser("user1@gelecek.com");
            await AddUser("user2@gelecek.com", Roles.NestleUser);
        }

        private async Task AddUser(string email, string role = Roles.Default)
        {
            if (await _userManager.FindByEmailAsync(email) == null)
            {
                var user = new ApplicationUser
                {
                    UserName = email,
                    Email = email,
                    Ad = "Admin",
                    Soyad = "Admin-",
                    SecurityStamp = "secure",
                    EmailConfirmed = true,
                };

                await _userManager.CreateAsync(user, "@1qa@1qa");
                await _userManager.AddToRoleAsync(user, role);
            }
        }

        private async Task SeedRoles()
        {
            if (!await _roleManager.RoleExistsAsync(Roles.Default))
                await _roleManager.CreateAsync(new IdentityRole(Roles.Default));

            var adminRole = new IdentityRole(Roles.Administrator);
            if (!await _roleManager.RoleExistsAsync(Roles.Administrator))
            {
                await _roleManager.CreateAsync(adminRole);

                adminRole = await _roleManager.FindByNameAsync(adminRole.Name);

                foreach (var claim in ApplicationPermissions.GetAllPermissionValues())
                {
                    await _roleManager.AddClaimAsync(adminRole,
                        new Claim(CustomClaimTypes.Permission, ApplicationPermissions.GetPermissionByValue(claim)));
                }

                 if (!await _roleManager.RoleExistsAsync (Roles.NestleUser)) {
                    await _roleManager.CreateAsync (new IdentityRole (Roles.NestleUser));
                }
            }
        }

        private async Task InitializeDatabase()
        {
            await _dbContext.Database.EnsureCreatedAsync();
        }
    }
}