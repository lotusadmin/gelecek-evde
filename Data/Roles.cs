using System;

namespace gelecek.Web.Data {
    internal static class Roles {
        public const string Default = "user";
        public const string Administrator = "administrator";
        public const string NestleUser = "nestleuser";
    }
}