using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gelecek.Web.Data.Model
{
    [Table("gelecek.Subscribe")]
    public class Subscribe 
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Email { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        public DateTime SendDate { get; set;}
    }
}

