using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using gelecek.Web.Requests;
using gelecek.Web.Requests.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace gelecek.Web.Controllers
{
    [AllowAnonymous] 
    [Route("api/[controller]")]
    public class SubscribeController : Controller
    {
        private readonly IMediator _mediator;

        public SubscribeController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Sistemdeki tüm Subscribeler
        /// </summary>
        /// <returns>IList</returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResultDto<SubscribeDto[]>), 200)]
        public async Task<ResultDto<SubscribeDto[]>> GetAll()
        {
            try
            {
                return await _mediator.Send(new Subscribe.GetSubscribes());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        
        /// <summary>
        /// Id ile Subscribe
        /// </summary>
        /// <returns>Subscribe</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResultDto<SubscribeDto>), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public async Task<ResultDto<SubscribeDto>> GetById(int id)
        {
            try
            {
                return await _mediator.Send(new Subscribe.GetSubscribebyId{ Id = id});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

               /// <summary>
        /// NestleSubscribe ekle
        /// </summary>
        /// <returns>NestleSubscribe</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResultDto<SubscribeDto>), 200)]
        public async Task<ResultDto<SubscribeDto>> Create([FromBody]SubscribePostDto post)
        {
            try
            {
                return await _mediator.Send(new Subscribe.SaveSubscribe{ Post = post});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
       
    }
}